// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <cstdlib>   // abs

//----------
// constants
//----------

const int sent_size = 4;

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator
{
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * <your documentation>
     */
    bool valid () const {
        // start iterator at very beginning
        // keep advancing --> if we hit the end (begin._p == a+N) correct!

        // <your code>

        const_iterator b = (*this).begin();
        const_iterator e = (*this).end();

        const int * _b = b.get_p();
        const int * _e = e.get_p();


        unsigned int c = 0;
        while(b != e && c < N)
        {
            ++c;
            ++b;
        }


        _e = e.get_p();
        _b = b.get_p();

        return b == e;


        // end: <your code>

    }

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            // <your code>
            return lhs._p == rhs._p;
            // end: <your code>
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        //Curtis: returns int reference; probably returns sentinel value
        int& operator * () const {
            // <your code>
            return *(reinterpret_cast<int*>(_p));   //get the int at address p
            // end: <your code>
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            // <your code>
            char * p = reinterpret_cast<char *>(_p);
            p += abs(*(*this)) + (2 * sent_size);    // advance to next sentinel
            _p = reinterpret_cast<int *>(p);
            return *this;
            // end: <your code>
        }

        // -----------
        // operator ++
        // -----------

        //NOTE: Undefined behavior if called at end()
        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        //NOTE: Undefined behavior if called at begin()
        iterator& operator -- () {
            // <your code>
            --_p;
            int curr_sent = *_p;
            char * cp = reinterpret_cast<char *>(_p);
            cp -= (curr_sent + sent_size);
            _p = reinterpret_cast<int *>(cp);

            return *this;
            // end: <your code>
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }

        int * get_p() {
            return _p;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const { // returns const int& so user can't mess with underlying values
            // <your code>
            return *_p;
            //end: <your code>
        }

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            // <your code>
            const char * p = reinterpret_cast<const char *>(_p);
            p += abs(*(*this)) + (2 * sent_size);    // advance to next sentinel
            _p = reinterpret_cast<const int *>(p);
            return *this;
            // end: <your code>
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            // <your code>
            --_p;
            const int curr_sent = *_p;
            char * cp = reinterpret_cast<char *>(_p);
            cp -= (curr_sent + sent_size);
            _p = reinterpret_cast<const int *>(cp);
            return *this;
            // end: <your code>
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }

        const int * get_p() const {
            return _p;
        }

    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        // <your code>
        // a[N] is created upon construction by default
        assert(N==1000);
        assert(sizeof(T) == 8);
        assert(sent_size == 4);
        if(N < (sizeof(T) + 2*sent_size))
        {
            std::bad_alloc ba;
            throw ba;
        }

        int num_bytes_allocated = N - (2 * sent_size);
        new_block(reinterpret_cast<pointer>(a), num_bytes_allocated, false);

        // end: <your code>
        assert(valid());
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid (bc there won't be enough space for anything)
     *
     * PRE: s is the number of T's to allocate, each of sizeof(T)
     *
     * POST: returns pointer to newly allocated block, with proper sentinels
     */
    pointer allocate (size_type s) {
        // <your code>

        iterator b = (*this).begin();
        iterator e = (*this).end();
        bool found = false;
        bool split = true;
        pointer out = nullptr;
        int bytes_to_allocate = s*sizeof(T);

        while(b != e && !found)
        {
            int curr_sent = *b;
            if(curr_sent >= bytes_to_allocate)                                              //must be positive, therefore free
            {
                found = true;
                out = reinterpret_cast<pointer>(&*b);                                       // the address of the current sentinel
                if((curr_sent - bytes_to_allocate) < (sizeof(T) + (2*sent_size))) {
                    bytes_to_allocate = curr_sent;                                          // give the whole block if the remainder would be useless
                    split = false;
                }
                new_block(out, bytes_to_allocate, true);

                // if necessary, create a free block using the remainder of the space from the current block
                if(split)
                {
                    char * char_out = reinterpret_cast<char *>(out);
                    new_block(reinterpret_cast<pointer>(char_out + (2*sent_size) + bytes_to_allocate), curr_sent - bytes_to_allocate - (2*sent_size), false);
                }
            }

            if(!found)
                ++b;
        }

        if(!found) {
            std::bad_alloc ba;
            throw ba;
        }


        //finally, out needs to point to the data, not the sentinel
        int * int_out = reinterpret_cast<int *>(out);
        ++int_out;
        out = reinterpret_cast<pointer>(int_out);
        assert(valid());
        return out;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */

    //POST: constructs an object in allocated storage; p points to it
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     *
     * PRE: p is a pointer to the beginning of the span we're deallocating (sentinel comes right before)
     *      s is the number of T's to deallocate, each of sizeof(T); if it doesn't match the sentinel: undefined
     *
     * POST: the span at p is now marked free, and has coallesced with
     *       with any adjacent free blocks
     */

    void deallocate (pointer p, size_type s) {
        //a + N
        // <your code>
        char * start = reinterpret_cast<char *>(p) - 4;
        int next_sent = -1;
        int last_sent = -1;

        int * curr_sent_ptr = reinterpret_cast<int *>(start);
        int curr_sent = *curr_sent_ptr;
        char * curr_block_ptr = reinterpret_cast<char *>(curr_sent_ptr);

        //we can't free an already free block
        assert(curr_sent < 0);
        curr_sent = abs(curr_sent);

        pointer new_block_start = reinterpret_cast<pointer>(start);
        int new_block_size = curr_sent;

        //if: we're not looking at the very first block
        if(start != a) {
            int * last_sent_ptr = curr_sent_ptr - 1;
            int last_sent = *last_sent_ptr;
            char * last_block_ptr = curr_block_ptr - (2*sent_size) - abs(last_sent);
            if(last_sent > 0) {
                new_block_start = reinterpret_cast<pointer>(last_block_ptr);
                new_block_size += (last_sent + (2 * sent_size));
            }
        }

        //if: we're not looking at the very last block
        if(curr_block_ptr + 2 * sent_size + curr_sent != a + N) {
            int * next_sent_ptr = reinterpret_cast<int *>(start + curr_sent + (2*sent_size));
            int next_sent = *next_sent_ptr;
            if(next_sent > 0) {
                new_block_size += (next_sent + (2 * sent_size));
            }
        }


        new_block(new_block_start, new_block_size, false);

        //end: <your code>

        assert(valid());
    }

    //PRE: i: int in range [1, number of blocks allocated]
    //POST: deallocated the ith block
    void deallocate_index(int i) {
        assert(i >= 1);

        iterator b = (*this).begin();

        // advance to the first busy block
        while(*b > 0)
            ++b;

        for(int j = 1; j < i; j++) {
            ++b;
            while(*b > 0)
                ++b;
        }

        deallocate(reinterpret_cast<pointer>(b.get_p() + 1), -1);
    }


    // ---------
    // new_block
    // ---------

    /**
    * PRE: num_bytes: number of bytes the user will be able to access
    *      start: location to start allocating them (sentinel will go here)
    *
    * POST: New block, with proper sentinels, placed at start
    *           negative: busy; positive: free
    *
    */
    void new_block(pointer s, int num_bytes, bool busy) {
        char * start = reinterpret_cast<char*>(s);
        assert(start >= a);
        assert(start <= (reinterpret_cast<char *>(a) + N));

        int sentinel = num_bytes;
        if(busy)
            sentinel *= -1;

        int * sentinel_placer = reinterpret_cast<int*>(start);
        *sentinel_placer = sentinel;                                // place start sentinel
        start+=(sent_size + num_bytes);               // advance to next sentinel location
        sentinel_placer = reinterpret_cast<int*>(start);
        *sentinel_placer = sentinel;                               // place end sentinel

        //DON'T assert(valid()) -- everything is not necessarily cleaned up at this point
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        int *_a = reinterpret_cast<int *>(a);
        return iterator(_a);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const { //need to convince that returning a pointer does not break our rule
        const int * _a = reinterpret_cast<const int *>(a);
        return const_iterator(_a);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        int * _e = reinterpret_cast< int *> (a + N);
        return iterator(_e);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        const int * _e = reinterpret_cast<const int *> (a + N);
        return const_iterator(_e);
    }
};

#endif // Allocator_h
