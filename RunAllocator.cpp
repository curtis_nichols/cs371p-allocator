// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout
#include <vector>   // vector
#include <sstream>  // string stream
#include <cstdlib>   // abs

#include "Allocator.hpp"

// ----
// main
// ----

int main () {
    using namespace std;
    /*
    your code for the read eval print loop (REPL) goes here
    in this project, the unit tests will only be testing Allocator.hpp, not the REPL
    the acceptance tests will be testing the REPL
    */

    istream& r = cin;
    ostream& o = cout;
    int num_tests = 0;
    r >> num_tests;
    vector<vector<int>> all_tests(num_tests);

    //read loop
    for(int t = 0; t < num_tests; ++t)
    {
        vector<int> curr_test(250); //only have to resize twice to hit max of 1000
        int request_num = 0;
        do {
            r >> curr_test[request_num++];
            char temp = r.get();
        }
        while(r && r.peek() != '\n'); //go until eof or new line
        all_tests[t] = curr_test;
    }

    for(int t = 0; t < num_tests; ++t) { 	// for every test
        vector<int> curr_test(all_tests[t]);
        my_allocator<double, 1000> ma;

        // make all allocation / deallocation requests
        for(int request_num = 0; curr_test[request_num] != 0; request_num++) {
            int val = curr_test[request_num];
            if(val >= 0) {
                ma.allocate(val);
            }
            else {
                ma.deallocate_index(val * -1);
            }
        }

        // print results
        my_allocator<double, 1000>::iterator b = ma.begin();
        my_allocator<double, 1000>::iterator e = ma.end();
        while(b != e) {
            o << *b << " ";
            ++b;
        }

        o << "\n";
    }

    return 0;
}
