[33mcommit 99d6c0925f2b19981be0691ad4f7f055acc2e7b6[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m
Author: Curtis Nichols <curtisn@cs.utexas.edu>
Date:   Sat Mar 28 11:29:29 2020 -0500

    acceptance tests; resolve #7

[33mcommit d9a08f71749e2ea35e2a78f296867a6703ad0e98[m
Author: Curtis Nichols <curtisn@cs.utexas.edu>
Date:   Fri Mar 27 11:38:46 2020 -0500

    fixed timout issue; pass all HR tests

[33mcommit c1de5672eee269e185aa799b303dba58e6de9d37[m
Author: Curtis Nichols <curtisn@cs.utexas.edu>
Date:   Wed Mar 25 16:43:39 2020 -0500

    passed initial tests

[33mcommit 667fc467027f6cd40b3770bdb7f0572ed4b35b0d[m
Author: Curtis Nichols <curtisn@cs.utexas.edu>
Date:   Thu Mar 12 11:38:07 2020 -0500

    read input succesfully

[33mcommit b1fa2a40cfe7714624d436a02ab95effec5e1862[m
Author: Glenn Downing <downing@cs.utexas.edu>
Date:   Sun Mar 8 14:23:23 2020 -0500

    another commit

[33mcommit 62d9cf7d02638db7899b4adfdd4dc026abed57b0[m
Author: Glenn Downing <downing@cs.utexas.edu>
Date:   Sun Mar 8 14:19:43 2020 -0500

    another commit

[33mcommit 4ea8706e80d0d94c0b97ef77067cfb201aa7c9ac[m
Author: Glenn Downing <downing@cs.utexas.edu>
Date:   Sun Mar 8 11:29:53 2020 -0500

    another commit

[33mcommit 900b898291f83cb069a010503703ae265f84186b[m
Author: Glenn Downing <downing@cs.utexas.edu>
Date:   Sun Mar 8 11:22:15 2020 -0500

    another commit

[33mcommit 48d11928e981cd1d42a9b775eef943e7ecc3641c[m
Author: Glenn Downing <downing@cs.utexas.edu>
Date:   Sun Mar 8 11:22:04 2020 -0500

    another commit

[33mcommit c5a28df50428625d497534d69ecc390432d121be[m
Author: Glenn Downing <downing@cs.utexas.edu>
Date:   Sun Mar 8 11:12:06 2020 -0500

    another commit

[33mcommit 833dd0aa206761eef40dc08986dec3fafa5a28e5[m
Author: Glenn Downing <downing@cs.utexas.edu>
Date:   Sat Mar 7 23:57:26 2020 -0600

    another commit

[33mcommit dfce527c3a14f60b9954168785162eab6571234f[m
Author: Glenn Downing <downing@cs.utexas.edu>
Date:   Sat Mar 7 23:50:23 2020 -0600

    another commit

[33mcommit 1aa2e4184a4c4cea980b0c98d8d731c0d258761e[m
Author: Glenn Downing <downing@cs.utexas.edu>
Date:   Sat Mar 7 23:32:42 2020 -0600

    another commit

[33mcommit a84dd1e263fbc8edb983868e029858e34e44dd18[m
Author: Glenn Downing <downing@cs.utexas.edu>
Date:   Sat Mar 7 23:31:47 2020 -0600

    first commit
