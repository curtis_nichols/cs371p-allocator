// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

using namespace testing;

// -----------------
// AllocatorFixture1
// -----------------

template <typename T>
struct AllocatorFixture1 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T; //type converter constructor of allocator
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_1 =
    Types<
    std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1);

TYPED_TEST(AllocatorFixture1, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type x;                   //makes an allocator<T>
    const size_type  s = 1;             //size of object is 1
    const value_type v = 2;             //make object with type converter constructor on 2
    const pointer    p = x.allocate(s); //allocate 1-- does that mean 1 object?
    if (p != nullptr) {
        x.construct(p, v);              //make p a pointer to v; how does this not lose the original p?
        ASSERT_EQ(v, *p);               //dereferencing p should give us v
        x.destroy(p);                   //destroy?
        x.deallocate(p, s);
    }
}           //deallocate 1 object? from the pointer p gave us

TYPED_TEST(AllocatorFixture1, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

//My Test: making a new block
TYPED_TEST(AllocatorFixture1, test2) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type x;                   // create a new allocator
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);

    if(p != nullptr) {
        x.construct(p, v);



        x.destroy(p);
        x.deallocate(p, s);
    }
}

// -----------------
// AllocatorFixture2
// -----------------

template <typename T>
struct AllocatorFixture2 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
};

using allocator_types_2 =
    Types<
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2);

TYPED_TEST(AllocatorFixture2, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture2, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// Make sure we can allocate one thing correctly, and then iterate through with valid
TYPED_TEST(AllocatorFixture2, test2) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;
    allocator_type x;

    const size_type s = 12;
    const int value_type_size = sizeof(value_type);

    x.allocate(s);
    //valid runs automatically

    ASSERT_EQ(x[0], s * value_type_size);
}

// we've now confirmed allocate works as expected, let's try to allocate multiple things
TYPED_TEST(AllocatorFixture2, test3) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type x;
    const int value_type_size = sizeof(value_type);

    x.allocate(1);
    x.allocate(2);
    x.allocate(3);

    typename my_allocator<value_type, 100>::iterator b = x.begin();
    ASSERT_EQ(*b, value_type_size * 1);
    ++b;
    ASSERT_EQ(*b, value_type_size * 2);
    ++b;
    ASSERT_EQ(*b, value_type_size * 3);
}

// finally, we'll try to deallocate some of the space we've allocated
TYPED_TEST(AllocatorFixture2, test4) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type x;
    const int value_type_size = sizeof(value_type);

    x.allocate(1);
    pointer p = x.allocate(2);
    x.allocate(3);
    x.deallocate(p, 2);

    typename my_allocator<value_type, 100>::iterator b = x.begin();
    ASSERT_EQ(*b, value_type_size * 1);
    ++b;
    ASSERT_EQ(*b, value_type_size * -2);
    ++b;
    ASSERT_EQ(*b, value_type_size * 3);
}
